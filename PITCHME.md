# What's new in `Aras`

*By Anthony P.*

---

## 1. Intro 

> Moved to `SAFe` ***S**caled **A**gile **F**ramework*  

> **CI** / **CD** pipelines

---

## 2. Supporting tools

> Tech docs Framework  

> Document comparison + Language translation

---

## 3. Systems Arch

> New item types 

> New controls / improvements => Structures, networks, ports, flows, ...

> 3D management

---

## 4. Domain Access Control

> Grant access to specific data

> Differents roles

> Can be combined with `Aras permissions`

---

## 5. UX ++

> Login  

> Faster

> Bases for future

---

## *Demo!*